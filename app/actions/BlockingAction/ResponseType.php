<?php


namespace App\actions\BlockingAction;


class ResponseType
{
    private $success = 200;
    private $bizFailed = 422;
    private $actionNotFound = 404;
    private $internalError = 500;
    private $badRequest = 400;

    /**
     * @return int
     */
    public function getSuccess(): int
    {
        return $this->success;
    }

    /**
     * @return int
     */
    public function getBizFailed(): int
    {
        return $this->bizFailed;
    }

    /**
     * @return int
     */
    public function getActionNotFound(): int
    {
        return $this->actionNotFound;
    }

    /**
     * @return int
     */
    public function getInternalError(): int
    {
        return $this->internalError;
    }

    /**
     * @return int
     */
    public function getBadRequest(): int
    {
        return $this->badRequest;
    }
}
