<?php


namespace App\actions\BlockingAction;


class ActionRequest
{

    private $bizUser;
    private $reqData;

    /**
     * @return mixed
     */
    public function getBizUser()
    {
        return $this->bizUser;
    }

    /**
     * @param mixed $bizUser
     */
    public function setBizUser($bizUser): void
    {
        $this->bizUser = $bizUser;
    }

    /**
     * @return mixed
     */
    public function getReqData()
    {
        return $this->reqData;
    }

    /**
     * @param mixed $reqData
     */
    public function setReqData($reqData): void
    {
        $this->reqData = $reqData;
    }

}
