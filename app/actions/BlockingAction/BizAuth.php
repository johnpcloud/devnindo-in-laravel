<?php


namespace App\actions\BlockingAction;


interface BizAuth
{
    public function validateUser($roleName);
}
