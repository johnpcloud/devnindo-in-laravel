<?php


namespace App\actions\BlockingAction;


interface Jsonable
{
    public function toJson();
}
