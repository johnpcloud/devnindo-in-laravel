<?php


namespace App\actions\BlockingAction;


use App\actions\errors\JsonDataFieldException;
use App\actions\errors\UserRoleException;
use App\actions\Validation\Violation;
use Firebase\JWT\JWT;
use Exception;
use Illuminate\Support\Facades\Request;
use Widmogrod\Monad\Either\Right;

abstract class BlockingAction implements BizAuth, ValidateWord
{

    //private $tokenKey = 'secret';

    abstract public function doBlockingBiz($validateRequest);

    abstract public function getSupportedUserRoles();

    abstract public function getDataValidationRequest();

    public function validateUser($roleName)
    {
        $request = Request::instance();
        $token = $request->bearerToken();

        $tokenKey = 'secret';
        $tokenAlg = 'HS256';


//        try {

            $payload = JWT::decode( $token, $tokenKey, array($tokenAlg));

            if(!property_exists($payload, 'role')) {
//                throw new UserRoleException('Role is missing');
                $role = str_split('role', 4);
                $violation = new Violation;
                return $violation->plain('ROLE_VIOLATION', $role);
            }

            if (!in_array($payload->role, $roleName)) {
//                throw new UserRoleException('Invalid Role');

                $role = str_split('role', 4);
                $violation = new Violation;
                return $violation->plain('ROLE_VIOLATION', $role);
            }

            return $payload;

//        } catch ( Exception $e) {
//            return $e->getMessage();
//        }

    }


    public function validateReqData($word)
    {
        $request = Request::instance();
        $getContent = $request->getContent();

        $decodedContent = json_decode($getContent);

        $key = $this->nonExistKeys($word, $decodedContent);

        if ($key) {
//            print_r($key);die();
//            throw new JsonDataFieldException($key.' not found' );

//            return Violation::object('KEY_VIOLATION', $key);
            $violation = new Violation;
            return $violation->object('KEY_VIOLATION', $key);
        }

        return $decodedContent;

    }

    public function validate($roleName, $data)
    {

        try {

            $bizUser = $this->validateUser($roleName);
//            print_r($bizUser);die();

//            if($bizUser instanceof Violation) {
            if(is_array($bizUser)) {
                return $bizUser;
            }

            $reqData = $this->validateReqData($data);
//            print_r($reqData);die();

//            if($reqData instanceof Violation) {
            if(is_array($reqData)) {
                return $reqData;
            }

            $actionRequest = new ActionRequest;
            $actionRequest->setBizUser($bizUser);
            $actionRequest->setReqData($reqData);


            return $actionRequest;

//            print_r($bizUser->getContent());
//            print_r($validateWordResult);
//            die();

        }
        catch (Exception $e) {
            return $e->getMessage();
        }

    }



    private function nonExistKeys(array $keys, $object) {
        //return !array_diff_key(array_flip($keys),(array) $arr);
        //print_r($keys);die();

        $missingKeys = array();
        foreach ($keys as $key) {
            if (!property_exists($object, $key)) {
                //print_r($key);die();
                //return $key;
                array_push($missingKeys, $key);
            }
        }
        if ($missingKeys != NULL) {
//            $missingKeys = implode(', ', $missingKeys);
            return $missingKeys;
        }
        return false;
    }

}
