<?php


namespace App\actions\BlockingAction;


class ActionManager
{
    public function doBiz(BlockingAction $action) {


        $validateResult = $action -> validate(
            $action->getSupportedUserRoles(),
            $action->getDataValidationRequest()
        );

        if($validateResult instanceof ActionRequest) {
            //print_r('Right');
            return $action -> doBlockingBiz($validateResult);

        } else {
            // Violation
            // return response by BizResponse->failed()

//            print_r(json_encode($validateResult));

            $bizResponse = new BizResponse();
            return $bizResponse->failed($validateResult);
        }



//        return $action -> doBlockingBiz($validateResult);

    }
}
