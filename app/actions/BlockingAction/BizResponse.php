<?php


namespace App\actions\BlockingAction;


class BizResponse extends ResponseType
{

//    public $bizUser;
//    public $reqData;

    /*public function __construct($bizUser,$reqData)
    {
        $this->bizUser = $bizUser;
        $this->reqData = $reqData;
    }*/

    public function failed($data)
    {
        /*$response = new \stdClass();
        $response->status =$this->getBizFailed();
        $response->responsedata =$data;*/

        $response = (object) [
            'STATUS' => $this->getBizFailed(),
            'RESPONSE DATA' => $data,
        ];

        return response()->json($response);
    }
}
