<?php


namespace App\actions\Validation;


class ViolationType
{
    private  $plainViolation = 'PLAIN_VIOLATION';
    private  $multiViolation = 'MULTI_VIOLATION';
    private  $objViolation   = 'OBJ_VIOLATION';

    /**
     * @return string
     */
    public function getPlainViolation(): string
    {
        return $this->plainViolation;
    }

    /**
     * @return string
     */
    public function getMultiViolation(): string
    {
        return $this->multiViolation;
    }

    /**
     * @return string
     */
    public function getObjViolation(): string
    {
        return $this->objViolation;
    }




}
