<?php


namespace App\actions\Validation;


class PlainViolation extends Violation
{

    public function __construct($constrainName, $required)
    {
        parent::__construct($constrainName, $required, $this->getPlainViolation());
    }

    /*protected function hasRequirement()
    {
        // TODO: Implement hasRequirement() method.

        return $this->required != NULL;
    }*/

}
