<?php


namespace App\actions\Validation;


class Violation extends ViolationType
{
    private $constrain;
    public $required;
    private $violationType;

    /*public function __construct($constrain, $required ,$violationType)
    {
        $this->constrain = $constrain;
        $this->required = $required;
        $this->violationType = $violationType;
    }*/

//    abstract protected function hasRequirement();



    public function plain($constrainName, $required)
    {
//        return new PlainViolation($constrainName,$required);

        $error=[];
        $errorMessage = [
            'constraint' => $constrainName,
            'type' => $this->getPlainViolation(),
            'required' => 'NOT_GIVEN'
        ];

        foreach ($required as $r) {
            $error[$r] = $errorMessage;
        }

        /*$errorMessage = new Violation;
        $errorMessage->constrain = $constrainName;
        $errorMessage->violationType = $this->getPlainViolation();
        $errorMessage->required = 'NOT_GIVEN';*/

        return $error;
    }

    public  function object($constrainName, $required)
    {

//        return new ObjViolation($constrainName, $required);

        $error = [
            'constraint' => $constrainName,
            'type' => $this->getObjViolation(),
            'required' => $this->plain('NOT_NULL', $required)
        ];
//                print_r(json_encode($error));die();

        /*$error = new Violation;
        $error->constrain = $constrainName;
        $error->violationType = $this->getObjViolation();
        $error->required = $this->plain('NOT_NULL', $required);*/

        return $error;
    }

    public static function multi($constrainName, $required)
    {
//        return new MultiViolation($constrainName, $required);
    }

}
