<?php


namespace App\actions\errors;

use UnexpectedValueException;

class JsonDataFieldException extends UnexpectedValueException
{

}
