<?php


namespace App\actions\user\query;


use App\actions\BlockingAction\BlockingAction;
use App\actions\user\validation\BizAuth;
use App\Http\Controllers\Controller;
use Exception;

class ShowUser extends BlockingAction
{

    //public $roleName = 'Admin';

    public function __construct()
    {

    }


    public function doBlockingBiz($validateResult)
    {

        $bizUser = $validateResult->getBizUser();
        $reqData = $validateResult->getReqData();

        return $bizUser->name;
    }

    public function getSupportedUserRoles()
    {
        return ['admin','user'];
    }

    public function getDataValidationRequest()
    {
        return [ 'id','name', 'state', 'city'];
    }
}
