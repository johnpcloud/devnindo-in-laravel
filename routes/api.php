<?php

use Illuminate\Http\Request;
use App\actions;
use App\actions\BlockingAction\ActionManager;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::post('/{module}.{actionType}.{actionName}', function () {

    $directory = request()->segment(count(request()->segments()));

    $dir = str_replace('.','\\',$directory);

    $className = 'App\\actions\\' . $dir;

    $controller = new $className;

    // initiate machine()
    $actionManager = new ActionManager;


    // return machine->doWork($controller, req, userdata);
    return $actionManager->doBiz($controller);

});
